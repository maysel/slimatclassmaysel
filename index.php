<?php
require "bootstrap.php";
use Chatter\Models\Message;
use Chatter\Models\User;
// use Chatter\Middleware\Logging;

$app = new \Slim\App();
// $app->add(new Logging());

$app->get('/hello/{name}', function($request, $response,$args){
   return $response->write('Hello '.$args['name']);
});

/*
$app->get('/customers/{cnumber}', function($request, $response,$args){
   return $response->write('Customer '.$args['cnumber']);
});

$app->get('/customers/{cnumber}/products/{pnumber}', function($request, $response,$args){
   return $response->write('Customer '.$args['cnumber' ]. ' Product '.$args['pnumber' ]);
});
*/
$app->get('/messages', function($request, $response,$args){
   $_message = new Message();
   $messages = $_message->all();
   $payload = [];
   foreach($messages as $msg){
        $payload[$msg->id] = [
            'user_id'=>$msg->user_id,
            'body'=>$msg->body,
            'created_at'=>$msg->created_at,
            'updated_at'=>$msg->updated_at
        ];
   }
   return $response->withStatus(200)->withJson($payload);
});


$app->get('/messages/{id}', function($request, $response,$args){
$_id = $args['id'];
$message = Message::find($_id);

   return $response->withStatus(200)->withJson($message);
});

$app->post('/messages', function($request, $response,$args){
    $message = $request->getParsedBodyParam('message','');
    $userid = $request->getParsedBodyParam('userid','');
   $_message = new Message();
   $_message->body = $message;
   $_message->user_id = $userid;
   $_message->save();
   if($_message->id){
       $payload = ['message_id'=>$_message->id];
       return $response->withStatus(201)->withJson($payload);
   }
   else{
       return $response->withStatus(400);
   }
});


$app->delete('/messages/{message_id}', function($request, $response,$args){
    $_message = Message::find($args['message_id']);
    $_message->delete();
    if($_message->exists){
        return $response->withStatus(400);
    }
    else{
         return $response->withStatus(200);
    }
});

$app->put('/messages/{message_id}', function($request, $response,$args){
    $message = $request->getParsedBodyParam('message','');
    $_message = Message::find($args['message_id']);
    //die("message id is " . $_message->id);
    $_message->body = $message;
    if($_message->save()){
        $payload = ['message_id'=>$_message->id,"result"=>"The message has been updated successfuly"];
        return $response->withStatus(200)->withJson($payload);
    }
    else{
         return $response->withStatus(400);
    }
});
/*
//messages bulk---------------------------------------------------------
$app->post('/messages/bulk', function($request, $response,$args){
    $payload = $request->getParsedBody();
    Message::insert($payload);
    return $response->withStatus(200)->withJson($payload);

});

$app->post('/users/bulk', function($request, $response,$args){
    $payload = $request->getParsedBody();//מה שאני מקבל זה רק ג'ייסון והפקודה הופכת את פיילווד למערך 
    User::insert($payload);
    return $response->withStatus(201)->withJson($payload);
});

$app->delete('/users/bulk', function($request, $response,$args){
    $payload = $request->getParsedBody();//מה שאני מקבל זה רק ג'ייסון והפקודה הופכת את פיילווד למערך 

    User::trash($payload);
    return $response->withStatus(201)->withJson($payload);
});
*/
/*
//user HW---------------------------------------------------------
$app->get('/users', function($request, $response,$args){
   $_user = new User();
   $users = $_user->all();
   $payload = [];
   foreach($users as $usr){
        $payload[$usr->id] = [
            'id'=>$usr->id,
            'username'=>$usr->username,
            'email'=>$usr->email
        ];
   }
   return $response->withStatus(200)->withJson($payload);
});

$app->post('/users', function($request, $response,$args){
    $username = $request->getParsedBodyParam('username','');
   $_user = new User();
   $_user->username = $username;
   $_user->save();
   if($_user->id){
       $payload = ['user id: '=>$_user->id];
       return $response->withStatus(201)->withJson($payload);
   }
   else{
       return $response->withStatus(400);
   }
});


$app->delete('/users/{id}', function($request, $response,$args){
    $user = User::find($args['id']);
    $user->delete();
    if($user->exists){
        return $response->withStatus(400);
    }
    else{
         return $response->withStatus(200);
    }
});

$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});



/// JWT - קבלה של שם משתמש וססמא,לבדוק זכאות מול המסד נתונים ולשלוח לקליינט את הג'ייסון המתאים

$app->post('/auth', function($request, $response,$args){
    $user = $request->getParsedBodyParam('user',''); //שליפת הערך של שם המשתמש מהנתונים שנשלחים בפוסט
    $password = $request->getParsedBodyParam('password',''); // שליפת הערך של הססמא
    // we need to check DB, for not hard coded
    if($user == 'jack' && $password == '1234' ){// בדיקת זכאות
    //we need to generate JWT but no now.
        //create JWT and send to client
        $payload = ['token'=>'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3IiwibmFtZSI6ImphY2siLCJhZG1pbiI6dHJ1ZX0.PETyPBssJTN1V8fSwPxkE0Hftg3hGYux4Rx1WcMC5NA'];
        return $response->withStatus(200)->withJson($payload);
    } 
    else{
        $payload = ['token'=>null];
          return $response->withStatus(403)->withJson($payload);
    }

});
*/

/*
$app->add(new \Slim\Middleware\JwtAuthentication([ // נכנס לפעולה אחרי שמגיע ראוט וג'יידבטי - בדיקת אימות
    "secret" => "supersecret",
    "path"=> ['/messages']// אלה הראוטים שאנחנו רוצים שהגיידבטי יפקח עליהם
]));*/ 


/*
$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});*/

$app->run();